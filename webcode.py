from flask import *
import pymysql
from werkzeug.utils import secure_filename
from AESCLASS import *

app=Flask(__name__)
app.secret_key='abc'

con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
cmd = con.cursor()
@app.route('/')
def login():
    return render_template("u-login.html")
@app.route('/log1',methods=['post'])
def log1():
    uname=request.form['textfield']
    psd=request.form['textfield2']
    cmd.execute("select * from login where username='"+uname+"' and password='"+psd+"'")
    s=cmd.fetchone()
    if s==None:
        return  '''<script> alert("Invalid Login"); window.location="/"</script>'''
    else:
      session['lid']=s[0]
    if s[3]=="univeristy":
        return  '''<script> alert("Login Success"); window.location="/university"</script>'''

    elif s[3] == "college":
        return '''<script> alert("Login Success"); window.location="/college"</script>'''
    else:
        return '''<script> alert("Invalid Login"); window.location="/"</script>'''


@app.route('/approve')
def approveregect():
    cmd.execute("SELECT college.* FROM college JOIN login ON college.login_id=login.id WHERE login.user_type='pending' ")
    s=cmd.fetchall()
    return render_template("u-approv_reregect.html",val=s)
@app.route('/approvedclgs')
def approvedclgs():
    cmd.execute("SELECT college.* FROM college JOIN login ON college.login_id=login.id WHERE login.user_type='college' ")
    s=cmd.fetchall()
    return render_template("approvedclgs.html",val=s)
@app.route('/approvecollege')
def approvecollege():
    id = request.args.get("id")
    cmd.execute("update login set user_type='college' where id='" + str(id) + "'")
    con.commit()
    return '''<script> alert("Approved"); window.location="/approve"</script>'''
@app.route('/rejectcollege')
def rejectcollege():
    id = request.args.get("id")
    cmd.execute("update login set user_type='rejected' where id='" + str(id) + "'")
    con.commit()
    return '''<script> alert("Rejected"); window.location="/approve"</script>'''
@app.route('/adddept')
def adddepartment():
    cmd.execute("select * from `college`")
    s=cmd.fetchall()
    return render_template("u-add_dept.html",val=s)

@app.route('/addcourse',methods=['get','post'])
def addcourse():
    cmd.execute("select * from department")
    s=cmd.fetchall()
    return render_template("u-add_course.html",val=s)

@app.route('/addsubject',methods=['get','post'])
def addsubject():
    cmd.execute("select * from department")
    s = cmd.fetchall()
    return render_template("u-add_subj.html",val=s)

@app.route('/addexamshedule',methods=['post'])
def addexamshedule():
    cmd.execute("select * from subject")
    s=cmd.fetchall()
    return render_template("u-add_exam_shedule.html",val=s)

@app.route('/uploadqp')
def uploadqp():
    cmd.execute("select * from exam_shedule")
    s = cmd.fetchall()
    return render_template("u-upld_qpaper.html",val=s)




@app.route('/regstaff',methods=['POST'])
def regstaff():
    return render_template("C-reg_staff.html")
@app.route('/VIEWstaff')
def VIEWstaff():
    cmd.execute("select * from staff where clgid='"+str(session['lid'])+"'")
    s = cmd.fetchall()
    return render_template("S-view_staff.html",val=s)

@app.route('/regstudent')
def regstudent():
    return render_template("C-reg_student.html")

@app.route('/viewexamshedule')
def viewexamshedule():
    return render_template("C-view_examshedule.html")

@app.route('/allocatestaff')
def allocatestaff():
    cmd.execute("select * from exam_shedule")
    s1 = cmd.fetchall()
    cmd.execute("select * from staff where clgid='"+str(session['lid'])+"'")
    s2 = cmd.fetchall()
    return render_template("C-staff_allocation.html",val=s1,vals=s2)

@app.route('/addnotification')
def addnotification():
    return render_template("C-add_notification.html")

@app.route('/addworkflow')
def addworkflow():
    return render_template("C-add_workflow.html")



@app.route('/viewstudent')
def viewstudent():
    return render_template("S-view_student.html")

@app.route('/viewworkflow')
def viewworkflow():
    return render_template("S-view_workflow.html")

@app.route('/viewnotification')
def viewnotification():
    return render_template("S-view_notification.html")

@app.route('/updatestatus')
def updatestatus():
    return render_template("S-update_status.html")

@app.route('/viewexamclass')
def viewexamclass():
    return render_template("S-view_examclass.html")

@app.route('/downloadqp')
def downloadqp():
    return render_template("S-dow_qp.html")

@app.route('/university')
def university():
    return render_template("U-link.html")

@app.route('/college')
def college():
    return render_template("C_link.html")
@app.route('/dept_rg',methods=['post'])
def dept_rg():
    clg=request.form['select']
    dept=request.form['textfield']
    cmd.execute("insert into department values(null,'"+dept+"','"+clg+"')")
    con.commit()
    return '''<script> alert("added"); window.location="/university"</script>'''
@app.route('/Manage_course')
def Manage_course():
    cmd.execute("SELECT `department`.`department_name`,`course`.`course_name` ,`course_id` FROM `course` JOIN `department` ON `department`.`department_id`=`course`.`department_id`")
    s=cmd.fetchall()
    return render_template("Manage_course.html",val=s)

@app.route('/del_crse')
def del_crse():
    id = request.args.get("id")
    cmd.execute("DELETE FROM `course` WHERE `course_id`='" + str(id) + "'")
    con.commit()
    return '''<script> alert("Deleted"); window.location="/Manage_course"</script>'''


@app.route('/crse_reg',methods=['post'])
def crse_reg():
    dept=request.form['select']
    crse=request.form['textfield']
    cmd.execute("insert into course values(null,'"+crse+"','"+dept+"')")
    con.commit()
    return '''<script> alert("added success"); window.location="/Manage_course"</script>'''

@app.route('/Manage_subjects')
def Manage_subjects():
    cmd.execute("SELECT `department`.`department_name`,`course`.`course_name`,`subject`.`subject_name`,`subject`.`subject_id` FROM  `subject` JOIN `course`ON `course`.`course_id`=`subject`.`course_id` JOIN `department` ON `department`.`department_id`=`course`.`department_id`")
    s=cmd.fetchall()
    return render_template("Manage_subjects.html",val=s)

@app.route('/del_subject')
def del_subject():
    id = request.args.get("id")
    cmd.execute("DELETE FROM `subject` WHERE `subject_id`='" + str(id) + "'")
    con.commit()
    return '''<script> alert("Deleted"); window.location="/Manage_subjects"</script>'''



@app.route('/course_select',methods=['get','post'])
def course_select():
    dept = request.form['dept']
    cmd.execute("SELECT * FROM `course` WHERE `department_id`='"+dept+"'")
    s=cmd.fetchall()
    lis=[0,'select']
    for r in s:
        lis.append(r[0])
        lis.append(r[1])
    print(lis)
    resp = make_response(jsonify(lis))
    resp.status_code = 200
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@app.route('/subjectreg',methods=['post'])
def subjectreg():
    crse=request.form['select2']
    subject=request.form['textfield']
    cmd.execute("insert into subject values(null,'"+subject+"','"+crse+"')")
    con.commit()
    return '''<script> alert("Registration Success"); window.location="/Manage_subjects"</script>'''

@app.route('/add_exam',methods=['post'])
def add_exam():
    subj=request.form['select']
    exam_date=request.form['textfield']
    exam_time=request.form['textfield2']
    cmd.execute("insert into exam_shedule values(null,'"+exam_date+"','"+exam_time+"','"+subj+"')")
    con.commit()
    return '''<script> alert("Registration Success"); window.location="/exam_schedules"</script>'''

@app.route('/add_qp',methods=['post'])
def add_qp():
    try:
        exm=request.form['select']
        qp=request.files['file']
        fname=secure_filename(qp.filename)
        qp.save('static/qpaper/'+fname)
        cmd.execute("select * from college")
        s=cmd.fetchall()
        import random
        list = []
        for i in s:
            lati=i[6]
            longi=i[7]
            for num in range(100, 10000):
                temp = num
                sum = 0
                while temp > 0:
                    digit = temp % 10
                    sum = sum + digit ** 3
                    temp = temp // 10

                if sum == num:
                    print (num)
                    list.append(num)
            print(list)
            random_num = random.choice(list)
            print(random_num)
            f = open("static/qpaper/"+fname, "rb")
            fileread=f.read()
            key=lati*longi*random_num
            key=str(key)
            pdata = base64.b64encode(fileread)
            print("enc start")
            et = encrypt(pdata.decode('ascii'), key)
            # encrypted_data=encrypt(b64,key)
            with open('static/encrypted/'+str(i[0])+"_"+fname, "wb") as file:
                file.write(et)
            encfile=str(i[0])+"_"+fname
            # dt = decrypt(et, key)
            # print("dt", dt)
            #
            #
            #
            # with open('static/decrypted/' + str(i[0]) + "_" + fname, "wb") as file2:
            #     # file2.write(dec)
            #     file2.write(base64.b64decode(dt))
            # pdata = get_byte_from_file('static/encrypted/' + encfile)
            # dt = decrypt(pdata, key)
            # with open('static/decrypted/' + encfile, "wb") as file:
            #     file.write(base64.b64decode(dt))
            cmd.execute("insert into question_paper values(null,'"+encfile+"','"+exm+"','"+str(i[1])+"','"+str(random_num)+"')")
            con.commit()
        return '''<script> alert("Registration Success"); window.location="/university"</script>'''
    except Exception as e:
        print("err",e)
        return '''<script> alert("Already added"); window.location="/university"</script>'''
@app.route('/exam_schedules')
def exam_schedules():
    cmd.execute("SELECT `exam_shedule`.* ,`subject`.`subject_name` FROM `subject` JOIN `exam_shedule` ON `exam_shedule`.`subject_id`=`subject`.`subject_id`")
    s=cmd.fetchall()
    return render_template("Exam_schedules.html",val=s)
@app.route('/edit_schedule')
def edit_schedule():
    id=request.args.get('id')
    session['eid']=id
    cmd.execute("SELECT * FROM `exam_shedule` WHERE `exam_shedule_id`='"+id+"'")
    s=cmd.fetchone()
    cmd.execute("select * from subject")
    s1 = cmd.fetchall()
    return render_template("editschedule.html",val=s,vals=s1)
@app.route('/update_exam',methods=['post'])
def update_exam():
    eid=session['eid']
    subj=request.form['select']
    exam_date=request.form['textfield']
    exam_time=request.form['textfield2']
    cmd.execute("update exam_shedule set exam_date='"+exam_date+"',exam_time='"+exam_time+"',subject_id='"+subj+"' where exam_shedule_id='"+str(eid)+"'")
    con.commit()
    return '''<script> alert("updated "); window.location="/exam_schedules"</script>'''

@app.route('/del_schedule')
def del_schedule():
    id = request.args.get("id")
    cmd.execute("DELETE FROM `exam_shedule` WHERE `exam_shedule_id`='" + str(id) + "'")
    con.commit()
    return '''<script> alert("Deleted"); window.location="/exam_schedules"</script>'''


@app.route('/complaint')
def complaint():
    con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
    cmd = con.cursor()
    cmd.execute("SELECT STAFF.name,complaints.* FROM staff JOIN complaints ON complaints.uid=staff.login_id WHERE complaints.comp_id NOT IN(SELECT reply.comp_id FROM reply)")
    result=cmd.fetchall()
    return render_template('complaints&reply.html',res=result)

@app.route('/reply')
def reply():
    comp_id=request.args.get('cid')
    session['c_id']=comp_id
    return render_template("Reply.html")

@app.route('/rep',methods=['post'])
def rep():
    reply=request.form['rp']
    comp_id=session['c_id']
    cmd.execute("insert into reply values (null,'"+str(comp_id)+"','"+reply+"')")
    con.commit()
    return '''<script>alert('replied!');window.location='/complaint'</script>'''

@app.route('/regclg')
def regclg():
    return render_template("College_register.html")



@app.route('/clgreg',methods=['post'])
def clgreg():
    try:
        clgname=request.form['textfield']
        place = request.form['textfield2']
        addrs = request.form['textfield3']
        phn = request.form['textfield4']
        lati = request.form['LAT']
        logi=request.form['LOGT']
        uname = request.form['textfield5']
        passwd=request.form['textfield6']
        cmd.execute("INSERT INTO login VALUES(null,'"+uname+"','"+passwd+"','pending')")
        lid=con.insert_id()
        cmd.execute("INSERT INTO college VALUES(null,'" + str(lid) + "','" + clgname + "','"+place+"','"+addrs+"','"+phn+"','"+lati+"','"+logi+"')")
        con.commit()
        return '''<script>alert('registered!');window.location='/'</script>'''
    except Exception as e:
        return '''<script>alert('already exist!');window.location='/'</script>'''

@app.route('/staffreg',methods=['post'])
def staffreg():
    try:
        name=request.form['textfield']
        gender = request.form['radiobutton']
        dob = request.form['textfield2']
        place = request.form['textfield3']
        post = request.form['textfield4']
        pin=request.form['textfield5']
        phone = request.form['textfield6']
        email=request.form['textfield7']
        uname = request.form['textfield8']
        passwd = request.form['textfield9']
        qualif=request.form.getlist('checkbox')
        qinfo=str.join(',',qualif)
        cmd.execute("INSERT INTO login VALUES(null,'"+uname+"','"+passwd+"','staff')")
        lid=con.insert_id()
        cmd.execute("INSERT INTO staff VALUES(null,'" + str(lid) + "','" + name + "','"+gender+"','"+dob+"','"+place+"','"+post+"','"+pin+"','"+phone+"','"+email+"','"+qinfo+"','"+str(session['lid'])+"')")
        con.commit()
        return '''<script>alert('registered!');window.location='/VIEWstaff'</script>'''
    except Exception as e:
        return '''<script>alert('already exist!');window.location='/regstaff'</script>'''

@app.route('/updatestaffreg',methods=['post'])
def updatestaffreg():
    try:
        name=request.form['textfield']
        gender = request.form['radiobutton']
        dob = request.form['textfield2']
        place = request.form['textfield3']
        post = request.form['textfield4']
        pin=request.form['textfield5']
        phone = request.form['textfield6']
        email=request.form['textfield7']
        qualif=request.form.getlist('checkbox')
        qinfo=str.join(',',qualif)
        lid=session['sid']
        cmd.execute("update staff set  name='" + name + "',gender='"+gender+"',dob='"+dob+"',place='"+place+"',post='"+post+"',pin='"+pin+"',phone='"+phone+"',email='"+email+"',qualification='"+qinfo+"' where login_id='"+str(lid)+"'")
        con.commit()
        return '''<script>alert('updated!');window.location='/VIEWstaff'</script>'''
    except Exception as e:
        return '''<script>alert('already exist!');window.location='/VIEWstaff'</script>'''
@app.route('/edit_staff')
def edit_staff():
    id=request.args.get('id')
    session['sid']=id
    cmd.execute("SELECT * FROM `staff` WHERE `login_id`='"+str(id)+"'")
    s=cmd.fetchone()
    return render_template("editstaff.html",val=s)


@app.route('/del_staff')
def del_staff():
    id = request.args.get("id")
    cmd.execute("DELETE FROM `staff` WHERE `login_id`='" + str(id) + "'")
    cmd.execute("delete from login where id='"+id+"'")
    con.commit()
    return '''<script> alert("Deleted"); window.location="/VIEWstaff"</script>'''


@app.route('/staff_allocate',methods=['post'])
def staff_allocate():
    try:
        examid=request.form['select']
        staffid=request.form['select2']
        # import random
        # list=[]
        # for num in range(100, 10000):
        #     temp = num
        #     sum = 0
        #     while temp > 0:
        #         digit = temp % 10
        #         sum = sum + digit ** 3
        #         temp = temp // 10
        #
        #     if sum == num:
        #         print (num)
        #         list.append(num)
        # print(list)
        # random_num = random.choice(list)

        cmd.execute("insert into allocate values (null,'"+str(examid)+"','"+str(staffid)+"','"+str(session['lid'])+"')")
        con.commit()
        return '''<script>alert('allocated!');window.location='/college'</script>'''
    except Exception as e:
        return '''<script>alert('already alloted!');window.location='/allocatestaff'</script>'''


@app.route('/C_viewdpts')
def C_viewdpts():
    cmd.execute("SELECT `department`.`department_name`,`course`.`course_name` ,`course_id` FROM `course` JOIN `department` ON `department`.`department_id`=`course`.`department_id` where `department`.`college_id`='"+str(session['lid'])+"'")
    s=cmd.fetchall()
    return render_template("C_viewdept.html",val=s)

@app.route('/allocatesubj')
def allocatesubj():
    cmd.execute("select * from subject")
    s1 = cmd.fetchall()
    cmd.execute("select * from staff where clgid='"+str(session['lid'])+"'")
    s2 = cmd.fetchall()
    return render_template("allocate_subect.html",val=s1,vals=s2)


@app.route('/subject_allocate',methods=['post'])
def subject_allocate():
    try:
        subid=request.form['select']
        staffid=request.form['select2']

        cmd.execute("insert into allot_subject values (null,'"+str(subid)+"','"+str(staffid)+"')")
        con.commit()
        return '''<script>alert('allocated!');window.location='/college'</script>'''
    except Exception as e:
        return '''<script>alert('error');window.location='/allocatesubj'</script>'''

def get_byte_from_file(filename):
    return open(filename,"rb").read()
if(__name__=='__main__'):
    app.run(debug=True)