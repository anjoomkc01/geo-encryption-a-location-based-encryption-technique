package com.example.geolocation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class update_profile extends AppCompatActivity {
EditText e1,e2,e3,e4,e5,e6,e7;
Button b1;
RadioButton r1,r2;
CheckBox c1,c2;
String gender="",qualif="",url;
SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        e1=findViewById(R.id.ed1);
        e2=findViewById(R.id.ed2);
        e3=findViewById(R.id.ed3);
        e4=findViewById(R.id.ed4);
        e5=findViewById(R.id.ed5);
        e6=findViewById(R.id.ed6);
        e7=findViewById(R.id.ed7);
        b1=findViewById(R.id.button);
        r1=findViewById(R.id.radioButton);
        r2=findViewById(R.id.radioButton2);
        c1=findViewById(R.id.checkBox);
        c2=findViewById(R.id.checkBox2);
        sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        url ="http://"+sp.getString("ip", "") + ":5000/viewprofile";
        RequestQueue queue = Volley.newRequestQueue(update_profile.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Display the response string.
                Log.d("+++++++++++++++++",response);
                try {

                    JSONArray ar=new JSONArray(response);



                    JSONObject jo=ar.getJSONObject(0);
                    e1.setText(jo.getString("name"));
                    e2.setText(jo.getString("dob"));
                    e3.setText(jo.getString("place"));
                    e4.setText(jo.getString("post"));
                    e5.setText(jo.getString("pin"));
                    e6.setText(jo.getString("phone"));
                    e7.setText(jo.getString("email"));
                    if(jo.getString("gender").equals("Male"))
                    {
                        r1.setChecked(true);
                    }
                    else
                    {
                        r2.setChecked(true);
                    }

                    String qlif=jo.getString("qualification");
                    String reslt[]=qlif.split(",");
                    for(int i=0;i<reslt.length;i++)
                    {
                        if(reslt[i].equals("degree"))
                        {
                            c1.setChecked(true);
                        }
                        if(reslt[i].equals("pg"))
                        {
                            c2.setChecked(true);
                        }

                    }







                } catch (Exception e) {
                    Log.d("=========", e.toString());
                    Toast.makeText(update_profile.this, e+"", Toast.LENGTH_SHORT).show();

                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(update_profile.this, "err"+error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //  params.put("complaint","reply");
                params.put("uid",sp.getString("lid",""));

                return params;
            }
        };
        queue.add(stringRequest);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name=e1.getText().toString();
                final String dob=e2.getText().toString();
                final String place=e3.getText().toString();
                final String post=e4.getText().toString();
                final String pin=e5.getText().toString();
                final String phn=e6.getText().toString();
                final String email=e7.getText().toString();

                if(r1.isChecked())
                {
                    gender=r1.getText().toString();
                }
                else
                {
                    gender=r2.getText().toString();
                }
                if(c1.isChecked())
                {
                    qualif=qualif+c1.getText().toString()+",";
                }
                if(c2.isChecked())
                {
                    qualif=qualif+c2.getText().toString()+",";
                }
                if(name.equalsIgnoreCase(""))
                {
                    e1.setError("Enter your first name ");
                }
                else if(!name.matches("^[a-zA-Z]*$"))
                {
                    e1.setError("characters allowed");
                }
                else if(dob.equalsIgnoreCase(""))
                {
                    e2.setError("Enter your dob");
                }
                else if(place.equalsIgnoreCase(""))
                {
                    e3.setError("Enter your place");
                }
                else if(post.equalsIgnoreCase(""))
                {
                    e4.setError("Enter your post");
                }
                else if(pin.equalsIgnoreCase(""))
                {
                    e5.setError("Enter your pincode");
                }
                else if(pin.length()!=6)
                {
                    e5.setError("invalid pin");
                    e5.requestFocus();


                }

                else if(phn.equalsIgnoreCase(""))
                {
                    e6.setError("Enter your phoneno");
                }
                else if(phn.length()!=10)
                {
                    e6.setError(" 10 nos required");
                    e6.requestFocus();


                }
                else if(email.equalsIgnoreCase(""))
                {
                    e7.setError("Enter your email");
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                {
                    e7.setError("Enter Valid Email");
                    e7.requestFocus();
                }
                else {
                    RequestQueue queue = Volley.newRequestQueue(update_profile.this);
                    url = "http://" + sp.getString("ip", "") + ":5000/updateprofile ";

                    // Request a string response from the provided URL.
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the response string.
                            Log.d("+++++++++++++++++", response);
                            try {
                                JSONObject json = new JSONObject(response);
                                String res = json.getString("task");

                                if (res.equalsIgnoreCase("Ok")) {
                                    Toast.makeText(getApplicationContext(), "updated", Toast.LENGTH_LONG).show();
                                    Intent ik = new Intent(getApplicationContext(), Userhome.class);
                                    startActivity(ik);
                                } else {


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            Toast.makeText(getApplicationContext(), "Error" + error, Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", name);
                            params.put("dob", dob);
                            params.put("place", place);
                            params.put("post", post);
                            params.put("pin", pin);
                            params.put("phn", phn);
                            params.put("email", email);
                            params.put("gender", gender);
                            params.put("qual", qualif);
                            params.put("lid", sp.getString("lid", ""));

                            return params;
                        }
                    };
                    queue.add(stringRequest);
                }

            }
        });

    }
}