from flask import *
import pymysql
app =  Flask(__name__)
from AESCLASS import *
app.secret_key="key"
con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
cmd = con.cursor()
@app.route('/logcode',methods=['post'])
def logcode():
    username=request.form['uname']
    password=request.form['pass']
    cmd.execute("select * from login where username='"+username+"'and password='"+password+"' and user_type='staff'")
    res=cmd.fetchone()
    if res is None:
        return jsonify({'task': "Invalid"})
    else:
        lid = res[0]
        return jsonify({'task': str(lid)})

@app.route('/assigned_subject',methods=['post'])
def assigned_subject():
    con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
    cmd = con.cursor()
    staffid=request.form['staffid']
    cmd.execute("SELECT `subject`.`subject_name` FROM `subject` JOIN `allot_subject` ON `allot_subject`.`subjid`=`subject`.`subject_id` WHERE `allot_subject`.`staffid`='"+staffid+"'")
    res = cmd.fetchall()
    row_headers = [x[0] for x in cmd.description]
    json_data = []
    for result in res:
        json_data.append(dict(zip(row_headers, result)))
    return jsonify(json_data)

@app.route('/assigned_exam',methods=['post'])
def assigned_exam():
    con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
    cmd = con.cursor()
    staffid=request.form['uid']
    cmd.execute("SELECT `exam_shedule`.`exam_date`,`exam_time`,`subject`.`subject_name` ,`question_paper`.`question_paper_name`,`question_paper`.`question_paper_id`FROM `allocate` JOIN  exam_shedule ON `allocate`.`examid`=`exam_shedule`.`exam_shedule_id` JOIN `subject` ON `subject`.`subject_id`=`exam_shedule`. `subject_id` JOIN  `question_paper` ON `question_paper`.`examid`=`exam_shedule`.`exam_shedule_id`  JOIN `staff` ON `staff`.`clgid`=`question_paper`.`clgid`AND `allocate`.`staffid`=`staff`.`login_id` WHERE `allocate`.`staffid`='"+staffid+"'")
    res = cmd.fetchall()
    row_headers = [x[0] for x in cmd.description]
    json_data = []
    for result in res:
        json_data.append(dict(zip(row_headers, result)))
    return jsonify(json_data)
@app.route('/complaint',methods=['post'])
def complaint():
    uid=request.form['uid']
    complaint=request.form['complaint']

    cmd.execute("insert into complaints values (null,'"+complaint+"',curdate(),'"+uid+"')")
    con.commit()
    return jsonify({'task': "Success"})



@app.route('/viewreply',methods=['post'])
def viewreply():
    con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
    cmd = con.cursor()
    id=request.form['id']
    cmd.execute("SELECT `complaints`.`complaint`,`complaints`.`date`,reply.reply FROM complaints JOIN reply ON reply.comp_id=complaints.comp_id WHERE complaints.uid='"+id+"' ")
    res = cmd.fetchall()
    row_headers = [x[0] for x in cmd.description]
    json_data = []
    for result in res:
        json_data.append(dict(zip(row_headers, result)))
    print(json_data)
    return jsonify(json_data)

@app.route('/viewprofile',methods=['post'])
def viewprofile():
    id=request.form['uid']
    con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
    cmd = con.cursor()
    cmd.execute(
        "SELECT * FROM `staff` WHERE login_id='" + id + "' ")
    res = cmd.fetchall()
    row_headers = [x[0] for x in cmd.description]
    json_data = []
    for result in res:
        json_data.append(dict(zip(row_headers, result)))
    return jsonify(json_data)



@app.route('/updateprofile', methods=['post'])
def updateprofile():
    lid=request.form['lid']
    name = request.form['name']
    gender=request.form['gender']
    dob=request.form['dob']
    place=request.form['place']
    post=request.form['post']
    pin=request.form['pin']
    phn=request.form['phn']
    email=request.form['email']
    qual = request.form.getlist('qual')
    qinfo = str.join(',', qual)
    cmd.execute("UPDATE `staff` SET `name`='"+name+"',`gender`='"+gender+"',`dob`='"+dob+"',`place`='"+place+"',`post`='"+post+"',`pin`='"+pin+"',`phone`='"+phn+"',`email`='"+email+"',`qualification`='"+qinfo+"' WHERE `login_id`='"+lid+"'")
    con.commit()
    return jsonify({'task': "Ok"})


@app.route('/download', methods=['post'])
def download():
    con = pymysql.connect(host='localhost', port=3306, user='root', password='', db='geo_location')
    cmd = con.cursor()
    lid=request.form['staffid']
    qpid=request.form['gpid']
    qname=request.form['qpaper']
    lati=request.form['lat']
    longt=request.form['longt']
    print(lati,longt)
    cmd.execute("SELECT `college`.`college_name`,`question_paper`.`armstrongno` ,college.`Lati`,`college`.`longi`,question_paper.clgid,(3959 * ACOS ( COS ( RADIANS('"+lati+"') ) * COS( RADIANS( `college`.`Lati`) ) * COS( RADIANS( `college`.`longi` ) - RADIANS('"+longt+"') ) + SIN ( RADIANS('"+lati+"') ) * SIN( RADIANS( `college`.`Lati` ) ))) AS user_distance FROM `college` JOIN question_paper ON `question_paper`.`clgid`=`college`.`login_id` JOIN `allocate` ON `allocate`.`examid`=`question_paper`.`examid`  AND  `college`.`login_id`=`allocate`.`clgid`   WHERE `allocate`.`staffid`='"+lid+"' and `question_paper`.`question_paper_id`='"+qpid+"' HAVING user_distance  < 6.2137")
    s=cmd.fetchone()
    if s is not None:
        armno=s[1]
        print(armno)
        clat=s[2]
        clongt=s[3]
        clgid=s[4]
        key=armno*clat*clongt
        key = str(key)
        print(qname)
        pdata=get_byte_from_file('static/encrypted/'+qname)
        dt=decrypt(pdata,key)
        with open('static/decrypted/' +  qname, "wb") as file:
            file.write(base64.b64decode(dt))
        return jsonify({'task':  'static/decrypted/' +  qname})

    else:
        return jsonify({'task': "no value"})



def get_byte_from_file(filename):
    return open(filename,"rb").read()


if __name__=='__main__':
    app.run(host='0.0.0.0',port=5000)